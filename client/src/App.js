import React, { useState } from 'react';
import Web3 from 'web3';
import { simpleStorageAbi } from './abi/abis.js';
import './App.css';


const web3 = new Web3(Web3.givenProvider);
const contractAddr = '0xc6775Af84e2FccBA70Bf1ec1CEb6297B30e999f7';
const SimpleContract = new web3.eth.Contract(simpleStorageAbi, contractAddr);

function App() {
  const [number, setNumber] = useState(0);
  const [getNumber, setGetNumber] = useState(0);
  const [hash, setHash] = useState(0);
  const [metadata, setMetadata] = useState(0);
  const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);

  const handleSet = async (e) => {
    e.preventDefault();
    const accounts = await window.ethereum.enable();
    const account = accounts[0];
    const mintNFT = await SimpleContract.methods.mintNFTasset(account, hash, metadata).send({from: account});
    
    
   
  }

  const handleGet = async (e) => {
    e.preventDefault();
    const result = await SimpleContract.methods.tokenURI(number).call();
    const URI = setGetNumber(result);
    console.log(result);
  }

  const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
		setIsFilePicked(true);
	};



  const uploadIPFS = async () => {

    const pinataApiKey = "7ccf49a0a69860c7daa9";
    const pinataSecretApiKey = "0541414b378b6dfd4f6e48f58f19dbd0e42532b8391e7f0d9a524af07425a761";
    const axios = require("axios");
    const fs = require("fs");
    const FormData = require("form-data");

  const pinFileToIPFS = async () => {
  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
  let data = new FormData();
  data.append("file", selectedFile);
  const res = await axios.post(url, data, {
    maxContentLength: "Infinity", 
    headers: {
      "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
      pinata_api_key: pinataApiKey, 
      pinata_secret_api_key: pinataSecretApiKey,
    },
  });
  console.log(res.data);
};
pinFileToIPFS();

  }
  


  return (
    <div className="App">
      <header className="App-header">

      <div>
			<input type="file" name="file" onChange={changeHandler} />
			{isFilePicked ? (
				<div>
					<p>Filename: {selectedFile.name}</p>
					<p>Filetype: {selectedFile.type}</p>
					<p>Size in bytes: {selectedFile.size}</p>
          
					<p>
						lastModifiedDate:{' '}
						{selectedFile.lastModifiedDate.toLocaleDateString()}
					</p>
				</div>
			) : (
				<p>Select a file to show details</p>
			)}
			<div>
				<button onClick={uploadIPFS}>Submit</button>
			</div>
		</div>
      
        <br/>

      <form onSubmit={handleSet}>
        <br/>
          <label>
            Hash Data
            <br/>
            <input type="text" name="name" value={hash} onChange={ e => setHash(e.target.value) }  />
            <br/>
            <br/>
            Metadata (IPFS link)
            <br/>
            <input type="text" name="name" value={metadata} onChange={ e => setMetadata(e.target.value) }  />
          </label>
        
        </form>
      

        <br/>
        <button onClick={handleSet} type="button">Mint</button>
        <br/>


        

        
      
        
        <form onSubmit={handleSet}>
        <br/>
          <label>
            Wich TokenId:
            <br/>
            <input type="text" name="name" value={number} onChange={ e => setNumber(e.target.value) }  />
          </label>
        
        </form>
        <br/>
        <button onClick={handleGet} type="button">get Metadata</button>

        <br/>
      <br/>
        { getNumber }
      </header>
     
      
    </div>
  );
  
}
export default App